import React from 'react';
const RightArrow = (props) =>{
    return (
        <span
        className="carousel__arrow carousel__arrow--right"
        onClick={props.onClick}
      >
        <span className="fa fa-2x fa-angle-right" />
      </span>
    )
}
export default RightArrow;