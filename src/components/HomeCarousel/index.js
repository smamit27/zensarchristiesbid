
import React from 'react';
import Carousel from './carousel'

const carouselSlidesData = [
  {
    content:
      "Tomorrow, you will be released. If you are bored of brawling with thieves and want to achieve something there is a rare blue flower that grows on the eastern slopes. Pick one of these flowers. If you can carry it to the top of the mountain, you may find what you were looking for in the first place.",
    author: "https://www.christies.com/media-library/home/featured-panels/2020/10/lap-amp/goya-desktop.jpg?w=1200",
    source: "facebook"
  }, {
    content:
      "You have learn to bury your guilt with anger. I will teach you to confront it and to face the truth.",
    author: "https://www.christies.com/media-library/home/featured-panels/2020/10/lap-amp/goya-desktop.jpg?w=1200",
    source: "Snapchat"
  }, {
    content:
      "Introduce a little anarchy, upset the established order and everything becomes chaos. I'm an agent of chaos. Oh, and you know the thing about chaos? It's fair.",
    author: "https://www.christies.com/media-library/home/featured-panels/2020/10/lap-amp/goya-desktop.jpg?w=1200",
    source: "facebook"
  }, {
    content:
      "I can't do that as Bruce Wayne... as a man. I'm flesh and blood. I can be ignored, destroyed. But as a symbol, I can be incorruptible, I can be everlasting.",
    author: "https://www.christies.com/media-library/home/featured-panels/2020/10/lap-amp/goya-desktop.jpg?w=1200",
    source: "facebook"
  }, {
    content:
      "But it's not who you are underneath... it's what you do that defines you.",
    author: "https://www.christies.com/media-library/home/featured-panels/2020/10/lap-amp/goya-desktop.jpg?w=1200",
    source: "twitter"
  }, {
    content:
      "When their enemies were at the gates the Romans would suspend democracy and appoint one man to protect the city. It wasn't considered an honor, it was a public service.",
    author: "https://www.christies.com/media-library/home/featured-panels/2020/10/lap-amp/goya-desktop.jpg?w=1200",
    source: "Google+"
  }, {
    content:
      "Master Wayne, you've been gone a long time. You look very fashionable. Apart from the mud.",
    author: "https://www.christies.com/media-library/home/featured-panels/2020/10/lap-amp/goya-desktop.jpg?w=1200",
    source: "twitter"
  }
];
 const HomeCarousel = () =>{

  return (
    <Carousel slides={carouselSlidesData} />
  )
 }
 export default HomeCarousel