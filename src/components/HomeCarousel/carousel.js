import React,{useState} from 'react'
import './homeStyle.css'
import LeftArrow from './left-arrow'
import RightArrow from './right-arrow'
import CarouselSlide from './carousel-slide';
import CarouselIndicator from './carousel-indicator'
const Carousel = (props) => {
    const [getIndex,setactiveIndex] = useState({
      activeIndex: 0
    })

  const goToSlide = (index) => {
    setactiveIndex({
      activeIndex: index
    });
  }

  const goToPrevSlide = (e) => {
    e.preventDefault();

    let index = getIndex.activeIndex;
    let { slides } = props;
    let slidesLength = slides.length;

    if (index < 1) {
      index = slidesLength;
    }

    --index;

    setactiveIndex({
      activeIndex: index
    });
  }

  const goToNextSlide = (e)=> {
    e.preventDefault();

    let index = getIndex.activeIndex;
    let { slides } = props;
    let slidesLength = slides.length - 1;
    console.log(slidesLength);
    if (index === slidesLength) {
      index = -1;
    }

    ++index;

    setactiveIndex({
      activeIndex: index
    });
  }

    return (
      <div className="carousel-container">
        <div className="carousel">
          <LeftArrow onClick={e => goToPrevSlide(e)} />

          <ul className="carousel__indicators">
          {props.slides.map((slide, index) =>
            <CarouselIndicator
              key={index}
              index={index}
              activeIndex={getIndex.activeIndex}
              isActive={getIndex.activeIndex===index} 
              onClick={e => goToSlide(index)}
            />
          )}
        </ul>

        <ul className="carousel__slides">
          {props.slides.map((slide, index) =>
            <CarouselSlide
              key={index}
              index={index}
              activeIndex={getIndex.activeIndex}
              slide={slide}
            />
          )}
        </ul>

        <RightArrow onClick={e => goToNextSlide(e)} />
      </div>
    </div>
    );
  
}

// Render Carousel component
export default Carousel;