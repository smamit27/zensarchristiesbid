import React from 'react';
const LeftArrow = (props) =>{
    return (
        <span
        className="carousel__arrow carousel__arrow--left"
        onClick={props.onClick}
      >
        
        <span className="fa fa-2x fa-angle-left" />
      </span>
    )
}
export default LeftArrow;