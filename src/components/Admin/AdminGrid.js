import React from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
//import axios from 'axios';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import WorkIcon from '@material-ui/icons/Work';
import TitleIcon from '@material-ui/icons/Title';

//Component inject
import Youtube from '../Gallery/Youtube';
import CommandGrid from './CommandGrid';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 200,
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'block'
    },
    posit: {
        position: 'absolute',
        right: '0px'
    },
    cardMedia: {
        padding: '15% !important',
        backgroundSize: 'contain',
        marginBottom: "10px" // 16:9
    },
    marginTop: {
        marginTop: '20px'
    },
    christies__header: {
        backgroundColor: 'transparent'
    }, '& > *': {
        margin: theme.spacing(1),
    },
    MarginButton: {
        marginRight: '10px',
        marginBottom: '10px'

    },
    card: {
        marginBottom: '10px',
        borderRadius: '10%',
        marginTop: '5px'
    }
}));


function AdminGrid(props) {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Container maxWidth="lg" className={classes.marginTop}>
                <Grid container>
                    <Grid item xs={12} md={6} lg={4} className="MuiPaper-elevation1">
                        <Youtube video="2lxPMExTQYA" autoplay="0" rel="0" modest="1" />

                        <Grid item xs={12} md={12} lg={12} className="MuiPaper-elevation1 ">
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image={props.propsValue.image_src}
                                    title="Image title" />
                            </Card>
                            <List className={classes.root}>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <TitleIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary="Sale Title" secondary={props.propsValue.title} />

                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <WorkIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary="Sale Number" secondary={props.propsValue.sale_number} />
                                </ListItem>

                            </List>

                        </Grid>
                    </Grid>


                    <Grid item xs={12} md={8} lg={8} className="MuiPaper-elevation1">
                        <TextField
                            label="Current Bid"
                            id="outlined-margin-normal-1"
                            defaultValue="50"
                            className={classes.textField}
                            margin="normal"
                            variant="outlined"
                            color="secondary"
                        />  <TextField
                            label="Next Bid"
                            id="outlined-margin-normal-2"
                            className={classes.textField}
                            value={props.propsValue.bidamount}
                            margin="normal"
                            variant="outlined"
                            color="primary"
                        />
                        <CommandGrid handlerAdminDollerValue={props.handlerAdminDollerValue}/>


                    </Grid>
                </Grid>

            </Container>
        </React.Fragment>
    )
}

export default AdminGrid
