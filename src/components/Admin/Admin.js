import React,{useState} from 'react';
import AdminGridView from './AdminGridView';
import Gallery from '../Gallery/Gallery';
import AdminGrid from './AdminGrid';

function Admin(props) {
    
    const [adminGridView,setadminGridView] = useState(false);
    const [propsValue,setpropsValue] = useState({});
    const adminGridViewHandler = (value) => {
        setpropsValue(value);
        setadminGridView(true);
        window.scrollTo({top: 0, behavior: "smooth"});

      }
      const handlerAdminDollerValue = (value) =>{
          console.log(value)
       setpropsValue({...propsValue,bidamount:propsValue.bidamount+value});
       window.scrollTo({top: 0, behavior: "smooth"});

    }
    return (
        <React.Fragment>
            {adminGridView ?
                 (<AdminGrid handlerAdminDollerValue={handlerAdminDollerValue} propsValue={propsValue}/>) : null}
            <AdminGridView adminGridViewHandler={adminGridViewHandler} />
            <Gallery />

        </React.Fragment>
    )
}

export default Admin
