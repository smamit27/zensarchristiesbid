import React from "react";
import styled from 'styled-components';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import RotateLeftSharpIcon from '@material-ui/icons/RotateLeftSharp';
import PanToolIcon from '@material-ui/icons/PanTool';
import Container from '@material-ui/core/Container';


const useStyles = makeStyles((theme) => ({
  root: {
      width: '100%',
      maxWidth: 200,
      backgroundColor: theme.palette.background.paper,
  },
  inline: {
      display: 'block'
  },
  posit: {
      position: 'absolute',
      right: '0px'
  },
  cardMedia: {
      padding: '15% !important',
      backgroundSize: 'contain',
      marginBottom: "10px" // 16:9
  },
  marginTop: {
      marginTop: '20px'
  },
  christies__header: {
      backgroundColor: 'transparent'
  }, '& > *': {
      margin: theme.spacing(1),
  },
  MarginButton: {
      marginRight: '10px',
      marginBottom: '10px'

  },
  card: {
      marginBottom: '10px',
      borderRadius: '10%',
      marginTop: '5px'
  },
  change__auction:{
    display: "block",
    textShadow: "2px 2px 0 rgba(0,0,0,0.05)",
    border: "0",
    background: "#EAC086",
    fontSize: "1rem",
    position:" relative",
    marginBottom: "10px",
    width:"90%",
    height: "40px"
  }
}));

function CommandGrid(props) {
  const classes = useStyles();
  const CommandGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1.25fr 1fr;
  grid-template-rows: repeat(3, 1fr);
  grid-gap: 3px;
  button {
    text-shadow: 2px 2px 0 rgba(0, 0, 0, 0.05);
    border: 0;
    background: #e0e0e0;
    border: 4px solid transparent;
    font-size: 1rem;
    position: relative;
    &:active {
      top: 2px;
    }
    &:focus {
      outline: 0;
      border-color: #ffffff;
    }
    &.takeoff {
      background: #41c7ff;
    }
    &.land {
      background: green;
      color:#ffffff;
    }
    &.emergency {
      background: orange;
      text-transform: uppercase;
      color: #ffffff;
    }
    &.rotate {
      background: #ffffff;
      color: black;
    }
    &.height {
      background: #fff;
      color: black;
    }
    span.symbol {
      display: block;
      font-size: 2rem;
      font-weight: 400;
    }
  }
  .center {
    display: grid;
    grid-gap: 3px;
    grid-template-columns: 1fr 1fr;
    button:last-child {
      grid-column: span 2;
    }
  }
  h2 {
    grid-column: 1 / -1;
    background: #ffc600;
    margin: 0;
    font-size: 1rem;
    text-align: center;
    padding: 0.5rem;
    color: #ffffff;
  }
  .withdraw{
    background: #A89684;
    color: #ffffff;
  }
  .far--warning{
    color: green;
  }
  .next--starnet{
    color: #ffffff;
    background: #D74130;
  }
  .next--room{
    color: #ffffff;
    background: #B30900;
  }
`;
  const sendCommand = () => {
    console.log(`Sending the command `);
  }

  return (
    <React.Fragment>
    <CommandGrid>
      <button onClick={sendCommand()}>
        <span className="symbol">
          <Tooltip title="Reset">
            <IconButton aria-label="Reset">
              <RotateLeftSharpIcon fontSize="large" />
            </IconButton>
          </Tooltip></span>
      </button>

      <button onClick={sendCommand()}>
        <span className="symbol">
          Lot 4
    </span>
      </button>
      <button className="rotate" onClick={sendCommand()} >
      <Tooltip title="Waiting for content">
            <IconButton aria-label="Waiting for content">
              <PanToolIcon fontSize="large" />
            </IconButton>
          </Tooltip>
      </button>
      <button onClick={sendCommand()}>
        <span className="symbol">
          <Tooltip title="Previous Lot">
            <IconButton aria-label="Previous Lot">
              <NavigateBeforeIcon fontSize="large" />
            </IconButton>
          </Tooltip>
        </span>
      </button>
      <div className="center">
        <button className="takeoff" onClick={sendCommand()}>
          Pass </button>
        <button className="land" onClick={sendCommand()}>
          SELL
      </button>
        <button className="emergency" onClick={sendCommand()}>
          Override
</button>
      </div>
      <button onClick={sendCommand()}>
        <span className="symbol"> 
        <Tooltip title="Next Lot">
          <IconButton aria-label="Next Lot">
            <NavigateNextIcon fontSize="large" />
          </IconButton>
        </Tooltip></span>
      </button>

      <button className=" withdraw" onClick={sendCommand()}>
        <span className="symbol"></span> Withdraw
    </button>
      <button className="rotate" onClick={sendCommand()}>
        <span className="symbol">High Bidder</span> 1400
    </button>
      <button onClick={sendCommand()}>
      <Tooltip title="Waiting for content">
            <IconButton aria-label="Waiting for content">
              <PanToolIcon fontSize="large" />
            </IconButton>
          </Tooltip>
    </button>
      <button className="far--warning" onClick={sendCommand()}>Far Warning</button>
      <button className="next--room" onClick={sendCommand()}>Next Room</button>
      <button className="next--starnet" onClick={sendCommand()}>Next | Starnet</button>
     
    </CommandGrid>
    <Container maxWidth="lg" className={classes.marginTop}>
    <Grid container>
      <Grid item xs={12} md={6} lg={4}>
      <button className={classes.change__auction} onClick={sendCommand()}>Change Auctioneer</button>
      <button className={classes.change__auction} onClick={sendCommand()}>Reopen Lot </button>
      <button className={classes.change__auction} onClick={sendCommand()}>Session End</button>
      <button className={classes.change__auction} onClick={sendCommand()}>Sale End</button>
      <button className={classes.change__auction} onClick={sendCommand()}>Clear Msg</button>
      </Grid>
    <Grid item xs={12} md={6} lg={8}>
    <Grid item>

      <Button variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(5) }}>5</Button>
      <Button variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(10) }}> 10</Button>
      <Button variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(20) }}> 20</Button>
      <Button variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(25) }}> 25</Button>
      <Button variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(30) }}> 30</Button>
      <Button variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(50) }}> 50</Button>
      </Grid>
      <Grid item>

    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(100) }}> 100</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(200) }}>200</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(300) }}> 300</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(500) }}> 500</Button>
    </Grid>
    <Grid item >

    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(1000) }}> 1000</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(2000) }}>2000</Button>
    <Button style={{width: "100px"}}variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(3000) }}> 3000</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(5000) }}> 5000</Button>
    </Grid>
    <Grid item >
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(10000) }}> 10000</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(20000) }}>20000</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(30000) }}> 30000</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(50000) }}> 50000</Button>
    </Grid>
    <Grid item >

    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(100000) }}> 100000</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(200000) }}>200000</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(300000) }}> 300000</Button>
    <Button style={{width: "100px"}} variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(500000) }}> 500000</Button>
   </Grid>
   <Grid item >

    <Button variant="contained" color="default" className={classes.MarginButton} onClick={() => { props.handlerAdminDollerValue(1000000) }}> 1000000</Button>
    </Grid>
</Grid>
</Grid>
</Container>

</React.Fragment>
  );
}

export default CommandGrid;