import React, { useState } from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
//import axios from 'axios';

import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import WorkIcon from '@material-ui/icons/Work';
import TitleIcon from '@material-ui/icons/Title';
import Button from '@material-ui/core/Button';
//Component inject
import Youtube from '../components/Gallery/Youtube';
import Gridview from '../components/Gridview';
import Gallery from '../components/Gallery/Gallery';


const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      maxWidth: 200,
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'block'
    },
    posit: {
      position: 'absolute',
      right: '0px'
    },
    cardMedia: {
      padding: '15% !important',
      backgroundSize: 'contain',
      marginBottom:"10px" // 16:9
    },
    marginTop: {
      marginTop: '20px'
    },
    christies__header: {
      backgroundColor: 'transparent'
    }, '& > *': {
      margin: theme.spacing(1),
    },
    MarginButton: {
      marginTop: '20px'
  
    },
  card: {
    marginBottom: '10px',
      borderRadius: '10%',
      marginTop: '5px'
  }
  }));

function Users(props) {
    const classes = useStyles();
    //const [bid, setBid] = useState([]);
    const [propsValue, setPropsValue] = useState({});
    const [gridviewShow, setgridviewShow] = useState(false);
    const gridViewHandler = (value) => {
      setPropsValue(value);
      setgridviewShow(true);
      window.scrollTo({top: 0, behavior: "smooth"});
    }
    const handlerDollerValue = (value) =>{
      setPropsValue({...propsValue,bidamount:propsValue.bidamount+value});
      
    }
    return (

<React.Fragment>
      {gridviewShow ? (
        <Container maxWidth="lg" className={classes.marginTop}>
          <Grid container>
            <Grid item xs={12} md={6} lg={6} className="MuiPaper-elevation1">
              <Youtube video="2lxPMExTQYA" autoplay="0" rel="0" modest="1" />
            </Grid>

            <Grid item xs={12} md={3} lg={3} className="MuiPaper-elevation1 ">
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image={propsValue.image_src}
                  title="Image title"
                />
                </Card>
                <List className={classes.root}>

                  <ListItem>
                    <ListItemAvatar>
                      <Avatar>
                        <TitleIcon />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Sale Title" secondary={propsValue.title} />

                  </ListItem>
                  <ListItem>
                    <ListItemAvatar>
                      <Avatar>
                        <WorkIcon />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Sale Number" secondary={propsValue.sale_number} />
                  </ListItem>
                 
                </List>

            </Grid>
         
            <Grid item xs={12} md={3} lg={3} className="MuiPaper-elevation1">
            <Typography component="h2" variant="h6" gutterBottom>
            Current Bid </Typography>
            <Typography component="p" variant="h4">
            ${propsValue.bidamount}
            </Typography>
       <Button variant="contained" color="secondary" className={classes.MarginButton} onClick={()=>{handlerDollerValue(10)}}> Next Bid $10</Button>
            </Grid>
          </Grid>

        </Container>

      ) : null}

      <Gridview gridViewHandler={gridViewHandler} />
      <Gallery />

      </React.Fragment>);
    
}

export default Users
