import React from 'react';
import Typography from '@material-ui/core/Typography';
import data from '../data/data.json';
function Gridview(props) {
    return (
        <div>
             <Typography component="h2" variant="h6" className="galleryheader" gutterBottom >
            Sale Information
            </Typography>
            <table className="table table-striped table-responsive-md table-active btn-table" >
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Sale Number</th>
                        <th>Sale Title</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Location</th>
                        <th>Live/Not Live</th>

                    </tr>
                </thead>
                <tbody>               
                {data.map(response =>
                    <tr key={response.sale_number}>
                    <th scope="row">            
                    {<img className="imageSrc" src={response.image_src} alt=""/>}</th>
                    <td >{response.sale_number}</td>
                    <td>{response.title}</td>
                    <td>{response.sale_start_date}</td>
                    <td>{response.sale_end_date}</td>
                    <td>{response.location}</td>

                    <td>
                        {response.streaming ? (
                        <button type="button" className="btn btn-outline-danger btn-sm m-0 waves-effect" onClick={() =>{props.gridViewHandler(response)}}>Live</button>
                        ): <div>Not Live</div> }
                    </td>
                    </tr>
                    )}
            </tbody>
            </table>
        </div>
    )
}

export default Gridview
