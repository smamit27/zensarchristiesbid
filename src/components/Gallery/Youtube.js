import React from 'react'

function Youtube(props) {
    const videoSrc = "https://www.youtube.com/embed/" +
    props.video + "?autoplay=" +
    props.autoplay + "&rel=" +
    props.rel + "&modestbranding=" +
    props.modest;
    return (
        <React.Fragment>     
    <div className="container">
        <iframe className="player" title="christies" type="text/html" width="100%" height="300px"
            src={videoSrc} frameBorder="0"/>
        </div>
</React.Fragment>    )
}

export default Youtube
