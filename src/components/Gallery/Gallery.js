import React, {  useState } from 'react';
import Slider from "react-slick";
//import axios from 'axios';
import './gallery.css';
import SimpleDialog from './dialog';
import Typography from '@material-ui/core/Typography';
import data from '../../data/data.json';


function Gallery(props) {
    //const [bid, setBid] = useState([]);
    const [open, setOpen] = useState(false);
    const [selectedValue, setSelectedValue] = useState([]);
    const [sendValue, setValue] = useState({});

    // useEffect(() => {
    //     axios.get(`http://zenbidsales.eastus.azurecontainer.io/livesales`)
    //         .then(res => {
    //             const bidDetails = res.data;
    //             setBid(bidDetails);
    //         })
    // }, []);
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 2
    }


    const handleClickOpen = (e) => {
        setOpen(true);
        setValue(e);
    };

    const handleClose = (value) => {
        setOpen(false);
        setSelectedValue(value);
    };

    return (
        <React.Fragment>
            <Typography component="h2" variant="h6" className="galleryheader" gutterBottom>
                Live Sale Content            
            </Typography>
            <Slider {...settings}>
                {data.map(bidResponse => <div key={bidResponse.image_src}>
                    <h1>
                    <img onClick={(e) => { handleClickOpen(bidResponse) }} src={bidResponse.image_src} alt="" /></h1></div>)}

            </Slider>

                <SimpleDialog selectedValue={selectedValue} sendValue={sendValue} open={open} onClose={handleClose} />
        </React.Fragment>

    );

}

export default Gallery
