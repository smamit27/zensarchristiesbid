import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { blue } from '@material-ui/core/colors';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import MuiDialogContent from '@material-ui/core/DialogContent';
import { withStyles } from '@material-ui/core/styles';
import DialogActions from '@material-ui/core/DialogActions';


const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  }, inline: {
    display: 'inline',
  },
  cardMedia: {
    paddingTop: '86.25%', // 16:9
  },
});

function SimpleDialog(props) {
  const classes = useStyles();
  const { onClose, selectedValue, open } = props;
  const handleClose = () => {
    onClose(selectedValue);
  };
    const DialogContent = withStyles((theme) => ({
    root: {
      padding: theme.spacing(2),
    },
  }))(MuiDialogContent);
 

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="customized-dialog-title" onClose={handleClose}>
      {props.sendValue.title}
    </DialogTitle>
    <DialogContent dividers>

      <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
              <Grid item  xs={12} sm={12} md={12}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={props.sendValue.image_src}
                    title="Image title"
                  />
                  <CardContent className={classes.cardContent}>
                  <Typography>
                    Sale Number: {props.sendValue.sale_number}
                    </Typography>
                    <Typography>
                     Start Date:  {props.sendValue.sale_start_date}
                    </Typography>
                    <Typography>
                     End Date:  {props.sendValue.sale_end_date}
                    </Typography>
                    <Typography>
                    Description: {props.sendValue.description}
                    </Typography>
                  </CardContent>
              </Card>
              </Grid>
          </Grid>
        </Container>
   </DialogContent>
   <DialogActions>
    </DialogActions>
    </Dialog>
  );
}

export default SimpleDialog;



