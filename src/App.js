import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './App.css';
import Admin from './components/Admin/Admin';
import Users from './components/Users';
import Header from "./components/Header";
import HomeCarousel from "./components/HomeCarousel";

function App(props) {

  // useEffect(() => {
  //   axios.get(`data`)
  //     .then(res => {
  //      const filterChart = res.data.filter(chart => chart.bidAmount >= 500);
  //      const slicePlotChart = filterChart.slice(0,1);
  //      const bidDetails = res.data.map(x => Number(x.bidAmount));
  //       setBid(slicePlotChart);

  //     })
  // }, []);


  return (

    <div className="App">
      <Header />
      
          <HomeCarousel >
          </HomeCarousel>       
  <Router>
        <Switch>
          <Route path="/users"><Users /></Route>
          <Route path="/admin"><Admin /></Route>
          <Route path="*"><div>Not found</div></Route>

        </Switch>
      </Router>

    </div>)

}


export default App;
